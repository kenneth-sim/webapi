package com.kenneth.webapi.graphql.utilities;

import com.kenneth.webapi.graphql.models.Department;
import com.kenneth.webapi.graphql.models.Employee;

import java.util.ArrayList;
import java.util.List;

public class InMemDb {
    public static List<Employee> employeeDb = new ArrayList<>();
    public static List<Department> departmentDb = new ArrayList<>();
}
