package com.kenneth.webapi.graphql.models;

public class AddEmployeeInput {
    private String name;

    private String salary;

    private Integer departmentId;

    public AddEmployeeInput() {
    }

    public AddEmployeeInput(String name, String salary, Integer departmentId) {
        this.name = name;
        this.salary = salary;
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return "AddEmployeeInput{" +
                "name='" + name + '\'' +
                ", salary='" + salary + '\'' +
                ", departmentId=" + departmentId +
                '}';
    }
}
