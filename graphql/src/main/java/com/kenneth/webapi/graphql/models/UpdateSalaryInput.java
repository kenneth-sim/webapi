package com.kenneth.webapi.graphql.models;

public class UpdateSalaryInput {
    private Integer employeeId;

    private String salary;

    public UpdateSalaryInput() {
    }

    public UpdateSalaryInput(Integer employeeId, String salary) {
        this.employeeId = employeeId;
        this.salary = salary;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "UpdateSalaryInput{" +
                "employeeId=" + employeeId +
                ", salary='" + salary + '\'' +
                '}';
    }
}
