package com.kenneth.webapi.graphql.controllers;

import com.kenneth.webapi.graphql.models.AddEmployeeInput;
import com.kenneth.webapi.graphql.models.Department;
import com.kenneth.webapi.graphql.models.Employee;
import com.kenneth.webapi.graphql.models.UpdateSalaryInput;
import com.kenneth.webapi.graphql.utilities.InMemDb;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.client.HttpGraphQlClient;
import org.springframework.graphql.data.method.annotation.*;
import org.springframework.graphql.server.WebGraphQlInterceptor;
import org.springframework.graphql.server.WebGraphQlRequest;
import org.springframework.graphql.server.WebGraphQlResponse;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
public class TestController {

    @Autowired
    private HttpGraphQlClient httpGraphQlClient;

    @PostConstruct
    public void init(){
        InMemDb.departmentDb.add(new Department(1, "IT", new ArrayList<>()));
        InMemDb.departmentDb.add(new Department(2, "HR", new ArrayList<>()));
        InMemDb.departmentDb.add(new Department(3, "Finance", new ArrayList<>()));
        InMemDb.departmentDb.add(new Department(4, "CS", new ArrayList<>()));
        InMemDb.departmentDb.add(new Department(5, "Security", new ArrayList<>()));
    }

    public Logger log = LoggerFactory.getLogger(TestController.class);

    @MutationMapping
    public Mono<Employee> addEmployee(@Argument AddEmployeeInput addEmployeeInput){
        Employee employee = mapping.apply(addEmployeeInput);
        InMemDb.employeeDb.add(employee);
        return Mono.just(InMemDb.employeeDb.stream().filter(e -> e.getId() == employee.getId()).findFirst().get());
    }

//    @QueryMapping
    @SchemaMapping(typeName = "Query", field="employeeById")
    public Flux<Employee> employeeById(@Argument Integer id){
        return Flux.just(InMemDb.employeeDb.stream().filter(e -> e.getId().equals(id)).findFirst().get());
    }

    public Mono<Employee> updateSalary(@Argument UpdateSalaryInput updateSalaryInput){
        OptionalInt index = IntStream.range(0, InMemDb.employeeDb.size())
                .filter(i -> InMemDb.employeeDb.get(i).getId() == updateSalaryInput.getEmployeeId())
                .findFirst();

        if(index.isPresent()) {
            InMemDb.employeeDb.get(index.getAsInt()).setSalary(updateSalaryInput.getSalary());
            return Mono.just(InMemDb.employeeDb.get(index.getAsInt()));
        }

        return null;
    }

    @QueryMapping
    public Flux<Department> allDepartment(){
        return Flux.fromIterable(InMemDb.departmentDb);
    }

    //this will have multiple invocations
//    @SchemaMapping(typeName = "Department", field = "employees")
//    public Flux<Employee> employees(Department department){
//        List<Employee> employees = InMemDb.employeeDb.stream().filter(e -> e.getDepartmentId() != department.getId()).collect(Collectors.toList());
//        return Flux.fromIterable(employees);
//    }

    //this will only have one invocation
    @BatchMapping
    public Mono<Map<Department, List<Employee>>> employees(List<Department> departments) {
        List<Integer> departmentIds = departments.stream()
                .map(Department::getId)
                .collect(Collectors.toList());

        Map<Integer, List<Employee>> employeesByDepartmentId = InMemDb.employeeDb.stream()
                .filter(e -> departmentIds.contains(e.getDepartmentId()))
                .collect(Collectors.groupingBy(Employee::getDepartmentId));

        Map<Department, List<Employee>> result = departments.stream()
                .collect(Collectors.toMap(
                        department -> department,
                        department -> employeesByDepartmentId.getOrDefault(department.getId(), List.of())
                ));

        return Mono.just(result);
    }

    @SubscriptionMapping
    public Flux<Employee> allEmployee(){
        return Flux.fromIterable(InMemDb.employeeDb).delayElements(Duration.ofSeconds(3));
    }

    Function<AddEmployeeInput, Employee> mapping = (data) -> {
        Integer id = new Random().nextInt();
        Employee employee = new Employee();
        employee.setId(id);
        employee.setName(data.getName());
        employee.setSalary(data.getSalary());
        employee.setDepartmentId(data.getDepartmentId());
//  this part is not needed since we use mapping for the schema
//        Department department = InMemDb.departmentDb.get(data.getDepartmentId());
//        if (department != null) {
//            // Get the list of employees
//            List<Employee> employees = department.getEmployees();
//
//            // If the list is null, initialize it
//            if (employees == null) {
//                employees = new ArrayList<>();
//                department.setEmployees(employees);
//            }
//
//            // Add the new employee to the list
//            employees.add(employee);
//        } else {
//            System.out.println("Department not found");
//        }

        return employee;
    };

    @GetMapping("employeeById/{id}")
    public Mono<List<Employee>> employeeById(@PathVariable String id){
        return this.httpGraphQlClient.document("query EmployeeById2 {\n" +
                        "    employeeById(id: " + id +
                        ") {\n" +
                        "        id\n" +
                        "        name\n" +
                        "        salary\n" +
                        "        departmentId\n" +
                        "    }\n" +
                        "}")
                .retrieve("employeeById")
                .toEntityList(Employee.class);
    }
}


@Component
class GraphqlServerInterceptor implements WebGraphQlInterceptor{

    private final Logger log = LoggerFactory.getLogger(GraphqlServerInterceptor.class);

    @Override
    public Mono<WebGraphQlResponse> intercept(WebGraphQlRequest request, Chain chain) {
        log.info("interceptor log {}", request.getDocument());
        return chain.next(request);
    }
}