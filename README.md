# Spring Boot Web API Architectural Design
This project covers usage of REST, gRPC, GraphQL, and SOAP in a Spring Boot application

## GraphQL
### Commands used
```
  grpcurl --plaintext localhost:9090 list
  grpcurl --plaintext localhost:9090 list com.kenneth.grpcserver.TestingService
  grpcurl --plaintext -d '{"id":"8192"}' localhost:9090 com.kenneth.grpcserver.TestingService.getTesting
```
### Misc
Ensure the spring version match with grpc version otherwise the app won't start. Current demo module has overridden 3.3.0 version in the parent pom as it is not supported yet.
### Reference
- [JavaInUse: Spring Boot + gRPC Hello World Example](https://www.javainuse.com/boot/grpc)
- [Mike's Spring Boot gRPC Starter](https://yidongnan.github.io/grpc-spring-boot-starter/en/server/getting-started.html)