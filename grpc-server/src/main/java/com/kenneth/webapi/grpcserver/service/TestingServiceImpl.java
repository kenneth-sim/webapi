package com.kenneth.webapi.grpcserver.service;

import com.kenneth.webapi.grpcserver.TestingRequest;
import com.kenneth.webapi.grpcserver.TestingResponse;
import com.kenneth.webapi.grpcserver.TestingServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class TestingServiceImpl extends TestingServiceGrpc.TestingServiceImplBase {
    @Override
    public void getTesting(TestingRequest request, StreamObserver<TestingResponse> responseObserver) {

        TestingResponse resp = TestingResponse.newBuilder().setId(request.getId()).setName("kenneth")
                .build();
        responseObserver.onNext(resp);
        responseObserver.onCompleted();
    }
}
