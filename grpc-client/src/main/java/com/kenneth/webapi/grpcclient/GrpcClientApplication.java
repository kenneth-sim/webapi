package com.kenneth.webapi.grpcclient;

import com.kenneth.webapi.grpcclient.service.TestingServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

//https://www.youtube.com/watch?v=SzJSgR1jhT8&ab_channel=JavaInUse

@SpringBootApplication
public class GrpcClientApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(GrpcClientApplication.class, args);
		TestingServiceImpl testingService = context.getBean(TestingServiceImpl.class);
		testingService.getTesting();
	}

}
