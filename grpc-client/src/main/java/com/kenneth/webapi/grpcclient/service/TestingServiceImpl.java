package com.kenneth.webapi.grpcclient.service;

import com.kenneth.webapi.grpcserver.TestingRequest;
import com.kenneth.webapi.grpcserver.TestingResponse;
import com.kenneth.webapi.grpcserver.TestingServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class TestingServiceImpl {
    public void getTesting() {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();

        TestingServiceGrpc.TestingServiceBlockingStub stub = TestingServiceGrpc.newBlockingStub(channel);

        TestingResponse bookResponse = stub.getTesting(TestingRequest.newBuilder().setId("1").build());

        System.out.println(bookResponse);

        channel.shutdown();
    }
}
